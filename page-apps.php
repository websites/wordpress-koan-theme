<?php /* Template Name: Software Template */ ?>
<?php get_header(); ?>

	<div id="primary" class="content-area stacked">
		<main id="main" class="site-main">
            <?php echo do_shortcode('[menu name="Software Menu" class="secondary-menu apps-menu"]'); ?>
            
            <div class="kontainer">
                <?php
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/content', 'page' );

                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;

                endwhile; // End of the loop.
                ?>
                <h2>License</h2>
                <p><?php echo get_the_title(); ?> is distributed under the terms of the <a href="http://www.gnu.org/licenses/old-licenses/gpl-2.0.html">GNU General Public License (GPL), Version 2.</a></p>
            </div><!-- .kontainer -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();