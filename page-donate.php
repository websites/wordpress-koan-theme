<?php /* Template Name: Donate Template */ ?>
<?php get_header(); ?>
    
    <div class="kontainer">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">

                <?php
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/content', 'page' );

                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;

                endwhile; // End of the loop.
                ?>
                
                <!-- Donate form -->
                <form 
                      action="https://www.paypal.com/en_US/cgi-bin/webscr" 
                      method="post" 
                      onsubmit="return amount.value >= 2 || window.confirm('Your donation is smaller than 2€. This means that most of your donation\nwill end up in processing fees. Do you want to continue?');">
                    <input type="hidden" name="cmd" value="_donations" />
                    <input type="hidden" name="lc" value="GB" />
                    <input type="hidden" name="item_name" value="Development and communication of KDE software" />
                    <input type="hidden" name="currency_code" value="EUR" />
                    <input type="hidden" name="cbt" value="Return to www.kde.org" />
                    <input type="hidden" name="return" value="https://www.kde.org/community/donations/thanks_paypal.php" />
                    <input type="hidden" name="notify_url" value="https://www.kde.org/community/donations/notify.php" />
                    <input type="hidden" name="business" value="kde-ev-paypal@kde.org" />
                    <input type='text' name="amount" value="30.00" id="donateAmountField" /> €
                    <button type='submit' id="donateSubmit">Donate via PayPal</button>
                </form>

            </main><!-- #main -->
        </div><!-- #primary -->

<?php
get_footer();
