<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Koan
 */

?>
        </div><!-- .kontainer -->
	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
        <div class="kontainer">
			<!-- Patrons of KDE -->
			<h3 class="center">KDE Patrons</h3>
			<div class="grid-section grid-4 patrons">
				<div>
					<img src="<?= get_template_directory_uri() ?>/media/patrons/canonical.svg" alt=" Canonical" title="Canonical">
				</div>
				<div>
					<img src="<?= get_template_directory_uri() ?>/media/patrons/google.svg" alt="Google" title="Google">
				</div>
				<div>
					<img src="<?= get_template_directory_uri() ?>/media/patrons/suse.svg" alt="SUSE" title="SUSE">
				</div>
				<div>
					<img src="<?= get_template_directory_uri() ?>/media/patrons/qt-company.svg" alt="The Qt Company" title="The Qt Company">
				</div>
				<div>
					<img src="<?= get_template_directory_uri() ?>/media/patrons/blue-systems.svg" alt="Blue Systems" title="Blue Systems">
				</div>
				<div>
					<img src="<?= get_template_directory_uri() ?>/media/patrons/enioka-haute-couture-logo.svg" alt="Enioka Haute Couture" title="Enioka Haute Couture">
				</div>
				<div>
					<img src="<?= get_template_directory_uri() ?>/media/patrons/slimbook.svg" alt="Slimbook" title="Slimbook">
				</div>
				<div>
					<img src="<?= get_template_directory_uri() ?>/media/patrons/pine64.svg" alt="Pine64" title="Pine64">
				</div>
			</div><!-- grid-section -->
            
            <!-- Footer Menus -->
            <div id="footer-sidebar">
                <div id="footer-sidebar1">
                    <?php
                    if(is_active_sidebar('footer-sidebar-1')){
                    dynamic_sidebar('footer-sidebar-1');
                    }
                    ?>
                </div>

                <div id="footer-sidebar2" class="grid-section grid-5 footer-menus">
                    <?php
                    if(is_active_sidebar('footer-sidebar-2')){
                    dynamic_sidebar('footer-sidebar-2');
                    }
                    ?>
                </div>
                <div id="footer-sidebar3">
                    <?php
                    if(is_active_sidebar('footer-sidebar-3')){
                    dynamic_sidebar('footer-sidebar-3');
                    }
                    ?>
                </div>
			</div>
        </div><!-- .kontainer -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<!-- Back to top -->
<script>
// When the user scrolls down 100px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
        document.getElementById("beamUp").style.display = "block";
    } else {
        document.getElementById("beamUp").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>
<button onclick="topFunction()" id="beamUp" title="Go to top"><i class="fa fa-angle-up"></i></button>
</body>
</html>
