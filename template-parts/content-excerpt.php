<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Koan
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php koan_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php koan_post_thumbnail(); ?>

	<div class="entry-content">
        <?php 
        if ( is_home () || is_category() || is_archive() ) {
            the_excerpt('');
        } 
        else {
        } // end of if statements
        ?>
	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
